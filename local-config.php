<?php

return [
    'name' => 'local',
    'default_local' => 'pl',
    'expire' => time() + 60 * 60 * 24 * 120, // 120 days
    'path' => '/',
    'domain' => '127.0.0.1',
    'security' => false, //true if can be sent only by https
    'http_only' => true, //not accessible for javascript

];
