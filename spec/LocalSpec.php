<?php

namespace spec\Paneric\Local;

use Paneric\Local\Local;
use PhpSpec\ObjectBehavior;

class LocalSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Local::class);
    }

    public function it_sets_value(): void
    {
        $config = [
            'name' => 'local',
            'default_local' => 'en',
            'expire' => time() + 60 * 60 * 24 * 120, // 120 days
            'path' => '/public',
            'domain' => '127.0.0.1:8080',
            'security' => false,
        ];

        $localMap = ['fr', 'nl', 'de', 'en', 'pl'];

        $this->setValue($config, $localMap, ['local' => 'de'], 'fr')->shouldReturn('fr');
        $this->setValue($config, $localMap, ['some' => 'some'], 'fr')->shouldReturn('fr');

        $this->setValue($config, $localMap, ['some' => 'some'])->shouldReturn('en');

        $this->setValue($config, $localMap, ['local' => 'en'])->shouldReturn('en');
    }
}
