<?php

declare(strict_types=1);

namespace Paneric\Local;

class Local
{
    private $value;

    public function setValue(array $config, array $localMap, array $cookies, string $local = null): ?string
    {
        if ($local !== null && in_array($local, $localMap)) {
            setcookie(
                $config['name'],
                $local,
                $config['expire'],
                $config['path'],
                $config['domain'],
                $config['security'],
                $config['http_only']
            );

            $this->value = $local;

            return $this->value;
        }

        if (!isset($cookies[$config['name']]) && $local === null) {

            setcookie(
                $config['name'],
                $config['default_local'],
                $config['expire'],
                $config['path'],
                $config['domain'],
                $config['security'],
                $config['http_only']
            );

            $this->value = $config['default_local'];

            return $this->value;
        }

        $local = $cookies[$config['name']];

        if (in_array($local, $localMap)) {
            return $local;
        }

        setcookie(
            $config['name'],
            $config['default_local'],
            $config['expire'],
            $config['path'],
            $config['domain'],
            $config['security'],
            $config['http_only']
        );

        $this->value = $config['default_local'];

        return $this->value;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }
}
